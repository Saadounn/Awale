/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package awalee;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Saadoun
 */
public class Acceuil extends javax.swing.JFrame {
    
    public int Table[] = new int[12];    
    public int Old_Table[] = new int[12];
    public int Score[] = new int[3];
    public int Old_Score[] = new int[3];

    
    public void initValues(){
        for(int i=0; i<12; i++){
            this.Table[i]=4;
        }
        Score[1] = 0;
        Score[2] = 0;
        for(int i=0; i<this.Table.length; i++) this.Old_Table[i] = this.Table[i];
        for(int i=0; i<this.Score.length; i++) this.Old_Score[i] = this.Score[i];

        ShowResult();

    }
    public void NewGame(){
        initValues();
    }
    
    /// 
    public void WriteOn(int i, int x) {
        switch(i){
            case 0 : X1.setText(""+x);break;
            case 1 : X2.setText(""+x);break;
            case 2 : X3.setText(""+x);break;
            case 3 : X4.setText(""+x);break;
            case 4 : X5.setText(""+x);break;
            case 5 : X6.setText(""+x);break;
            case 6 : X7.setText(""+x);break;
            case 7 : X8.setText(""+x);break;
            case 8 : X9.setText(""+x);break;
            case 9 : X10.setText(""+x);break;
            case 10: X11.setText(""+x);break;
            case 11: X12.setText(""+x);break;
        }
    }
    
    public void Hover(int x){
        x = x%12;
        if(x % 12 == 0) x = 12;
        switch(x){
            case 1 : X1.setBackground(Color.getHSBColor(230, 230, 230));break;
            case 2 : X2.setBackground(Color.getHSBColor(230, 230, 230));break;
            case 3 : X3.setBackground(Color.getHSBColor(230, 230, 230));break;
            case 4 : X4.setBackground(Color.getHSBColor(230, 230, 230));break;
            case 5 : X5.setBackground(Color.getHSBColor(230, 230, 230));break;
            case 6 : X6.setBackground(Color.getHSBColor(230, 230, 230));break;
            case 7 : X7.setBackground(Color.getHSBColor(230, 230, 230));break;
            case 8 : X8.setBackground(Color.getHSBColor(230, 230, 230));break;
            case 9 : X9.setBackground(Color.getHSBColor(230, 230, 230));break;
            case 10: X10.setBackground(Color.getHSBColor(230, 230, 230));break;
            case 11: X11.setBackground(Color.getHSBColor(230, 230, 230));break;
            case 12: X12.setBackground(Color.getHSBColor(230, 230, 230));break;
        }
    }
    public void UnHover(){
    X1.setBackground(new Color(220,220,220));
    X2.setBackground(new Color(220,220,220));
    X3.setBackground(new Color(220,220,220));
    X4.setBackground(new Color(220,220,220));
    X5.setBackground(new Color(220,220,220));
    X6.setBackground(new Color(220,220,220));
    X7.setBackground(new Color(220,220,220));
    X8.setBackground(new Color(220,220,220));
    X9.setBackground(new Color(220,220,220));
    X10.setBackground(new Color(220,220,220));
    X11.setBackground(new Color(220,220,220));
    X12.setBackground(new Color(220,220,220));
    }
    public void ShowResult(){
        for(int i=0; i<12; i++){
            WriteOn(i,this.Table[i]);
            J1.setText("Joueur 1 : "+this.Score[1]);
            J2.setText("Joueur 2 : "+this.Score[2]);

        }
    }
        //fonction appelé lors du clique sur une case 
        public void Click(int num_case){
            int val = Table[num_case-1]; 
            // sauvegarde du dernier êtat de la table de jeu (pour une annulation du coup joué)
            if(val != 0)
                for(int i=0; i<this.Table.length; i++) this.Old_Table[i] = this.Table[i];  
            
            int last_case = (num_case + val -1)%12; //derniére case modifiée
            int scor =0;
            
            //mettre à 0 la case cliquée et distribuer les pierres une dans chaque case qui suit
            this.Table[num_case-1]=0;
            int i=num_case;
            while(val > 0){
                if(num_case-1 != i){
                    (this.Table[i])++;
                    val--;
                }
                i++; 
                if(i == 12 ) i=0;
            }
            //calcule du score
            while((last_case > 5 && last_case < 12) && (this.Table[last_case] == 3 || this.Table[last_case] == 2)){
                scor = scor + this.Table[last_case];
                this.Table[last_case] = 0;
                last_case--;
            }
            //sauvegarde du dernier êtat du score pour une annulation du coup potentielle
            this.Old_Score[1] = this.Score[1]; this.Old_Score[2] = this.Score[2];
            this.Score[2] += scor;
        }
    /**
     * Creates new form Acceuil
     */
    public Acceuil() {
        
        initComponents();
        NewGame();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Undo = new javax.swing.JButton();
        J1 = new javax.swing.JLabel();
        J2 = new javax.swing.JLabel();
        X1 = new javax.swing.JTextField();
        X2 = new javax.swing.JTextField();
        X3 = new javax.swing.JTextField();
        X4 = new javax.swing.JTextField();
        X5 = new javax.swing.JTextField();
        X6 = new javax.swing.JTextField();
        X7 = new javax.swing.JTextField();
        X8 = new javax.swing.JTextField();
        X10 = new javax.swing.JTextField();
        X11 = new javax.swing.JTextField();
        X12 = new javax.swing.JTextField();
        X9 = new javax.swing.JTextField();
        NewGame = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        Undo.setText("Undo");
        Undo.setEnabled(false);
        Undo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                UndoActionPerformed(evt);
            }
        });

        J1.setText("Joueur 1 : 0");

        J2.setText("Joueur 2 : 0");

        X1.setEditable(false);
        X1.setBackground(new java.awt.Color(220, 220, 220));
        X1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        X1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        X1.setFocusable(false);
        X1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                X1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                X1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                X1MouseExited(evt);
            }
        });
        X1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                X1ActionPerformed(evt);
            }
        });

        X2.setEditable(false);
        X2.setBackground(new java.awt.Color(220, 220, 220));
        X2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        X2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        X2.setFocusable(false);
        X2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                X2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                X2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                X2MouseExited(evt);
            }
        });
        X2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                X2ActionPerformed(evt);
            }
        });

        X3.setEditable(false);
        X3.setBackground(new java.awt.Color(220, 220, 220));
        X3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        X3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        X3.setFocusable(false);
        X3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                X3MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                X3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                X3MouseExited(evt);
            }
        });
        X3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                X3ActionPerformed(evt);
            }
        });

        X4.setEditable(false);
        X4.setBackground(new java.awt.Color(220, 220, 220));
        X4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        X4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        X4.setFocusable(false);
        X4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                X4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                X4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                X4MouseExited(evt);
            }
        });
        X4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                X4ActionPerformed(evt);
            }
        });

        X5.setEditable(false);
        X5.setBackground(new java.awt.Color(220, 220, 220));
        X5.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        X5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        X5.setFocusable(false);
        X5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                X5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                X5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                X5MouseExited(evt);
            }
        });
        X5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                X5ActionPerformed(evt);
            }
        });

        X6.setEditable(false);
        X6.setBackground(new java.awt.Color(220, 220, 220));
        X6.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        X6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        X6.setFocusable(false);
        X6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                X6MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                X6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                X6MouseExited(evt);
            }
        });
        X6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                X6ActionPerformed(evt);
            }
        });

        X7.setEditable(false);
        X7.setBackground(new java.awt.Color(220, 220, 220));
        X7.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        X7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        X7.setFocusable(false);
        X7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                X7ActionPerformed(evt);
            }
        });

        X8.setEditable(false);
        X8.setBackground(new java.awt.Color(220, 220, 220));
        X8.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        X8.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        X8.setFocusable(false);
        X8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                X8ActionPerformed(evt);
            }
        });

        X10.setEditable(false);
        X10.setBackground(new java.awt.Color(220, 220, 220));
        X10.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        X10.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        X10.setFocusable(false);
        X10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                X10ActionPerformed(evt);
            }
        });

        X11.setEditable(false);
        X11.setBackground(new java.awt.Color(220, 220, 220));
        X11.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        X11.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        X11.setFocusable(false);
        X11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                X11ActionPerformed(evt);
            }
        });

        X12.setEditable(false);
        X12.setBackground(new java.awt.Color(220, 220, 220));
        X12.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        X12.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        X12.setFocusable(false);
        X12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                X12ActionPerformed(evt);
            }
        });

        X9.setEditable(false);
        X9.setBackground(new java.awt.Color(220, 220, 220));
        X9.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        X9.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        X9.setFocusable(false);
        X9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                X9ActionPerformed(evt);
            }
        });

        NewGame.setText("New");
        NewGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NewGameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(J1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(X1)
                                    .addComponent(X12, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(X2)
                                    .addComponent(X11, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(X3)
                                    .addComponent(X10, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(X9, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)
                                    .addComponent(X4))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(X8, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(X5)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(J2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(NewGame)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Undo)
                            .addComponent(X6, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(X7, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {X10, X11, X12, X7, X8, X9});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addComponent(J1)
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(X1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(X12, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(53, 53, 53)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(X11, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(X2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(X10, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(X8, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(X9, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(X7, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(X3, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(X4, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(X5, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(X6, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(J2)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Undo)
                        .addComponent(NewGame)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {X1, X10, X11, X12, X2, X3, X4, X5, X6, X7, X8, X9});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void UndoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_UndoActionPerformed
        for(int i=0; i<this.Table.length; i++) this.Table[i] = this.Old_Table[i];
        for(int i=0; i<this.Score.length; i++) this.Score[i] = this.Old_Score[i];
        Undo.setEnabled(false);
        ShowResult();
    }//GEN-LAST:event_UndoActionPerformed

    private void X1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_X1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_X1ActionPerformed

    private void X4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_X4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_X4ActionPerformed

    private void X5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_X5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_X5ActionPerformed

    private void X2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_X2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_X2ActionPerformed

    private void X7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_X7ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_X7ActionPerformed

    private void X8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_X8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_X8ActionPerformed

    private void X3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_X3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_X3ActionPerformed

    private void X10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_X10ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_X10ActionPerformed

    private void X11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_X11ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_X11ActionPerformed

    private void X12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_X12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_X12ActionPerformed

    private void X9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_X9ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_X9ActionPerformed

    private void X6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_X6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_X6ActionPerformed

    private void NewGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NewGameActionPerformed
        NewGame();
        Undo.setEnabled(false);
    }//GEN-LAST:event_NewGameActionPerformed

    private void X1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X1MouseEntered
         int val = Integer.parseInt(X1.getText());
         Hover(val+1);
    }//GEN-LAST:event_X1MouseEntered

    private void X1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X1MouseExited
        UnHover();
    }//GEN-LAST:event_X1MouseExited

    private void X2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X2MouseEntered
         int val = Integer.parseInt(X2.getText());
         Hover(val+2);
    }//GEN-LAST:event_X2MouseEntered

    private void X2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X2MouseExited
        UnHover();
    }//GEN-LAST:event_X2MouseExited

    private void X3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X3MouseEntered
         int val = Integer.parseInt(X3.getText());
         Hover(val+3);
    }//GEN-LAST:event_X3MouseEntered

    private void X3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X3MouseExited
        UnHover();
    }//GEN-LAST:event_X3MouseExited

    private void X4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X4MouseEntered
         int val = Integer.parseInt(X4.getText());
         Hover(val+4);
    }//GEN-LAST:event_X4MouseEntered

    private void X4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X4MouseExited
        UnHover();
    }//GEN-LAST:event_X4MouseExited

    private void X5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X5MouseEntered
         int val = Integer.parseInt(X5.getText());
         Hover(val+5);
    }//GEN-LAST:event_X5MouseEntered

    private void X5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X5MouseExited
        UnHover();
    }//GEN-LAST:event_X5MouseExited

    private void X6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X6MouseEntered
         int val = Integer.parseInt(X6.getText());
         Hover(val+6);
    }//GEN-LAST:event_X6MouseEntered

    private void X6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X6MouseExited
        UnHover();
    }//GEN-LAST:event_X6MouseExited

    private void X1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X1MouseClicked
 
        Click(1);
        ShowResult();
        Undo.setEnabled(true);        
    }//GEN-LAST:event_X1MouseClicked

    private void X2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X2MouseClicked
        Click(2);
        ShowResult();
        Undo.setEnabled(true);      
    }//GEN-LAST:event_X2MouseClicked

    private void X3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X3MouseClicked
        Click(3);
        ShowResult();
        Undo.setEnabled(true);        
    }//GEN-LAST:event_X3MouseClicked

    private void X4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X4MouseClicked
        Click(4);
        ShowResult();
        Undo.setEnabled(true);        
    }//GEN-LAST:event_X4MouseClicked

    private void X5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X5MouseClicked
        Click(5);
        ShowResult();
        Undo.setEnabled(true);        
    }//GEN-LAST:event_X5MouseClicked

    private void X6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_X6MouseClicked
        Click(6);  
        ShowResult();
        Undo.setEnabled(true);
    }//GEN-LAST:event_X6MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Acceuil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Acceuil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Acceuil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Acceuil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Acceuil().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel J1;
    private javax.swing.JLabel J2;
    private javax.swing.JButton NewGame;
    private javax.swing.JButton Undo;
    private javax.swing.JTextField X1;
    private javax.swing.JTextField X10;
    private javax.swing.JTextField X11;
    private javax.swing.JTextField X12;
    private javax.swing.JTextField X2;
    private javax.swing.JTextField X3;
    private javax.swing.JTextField X4;
    private javax.swing.JTextField X5;
    private javax.swing.JTextField X6;
    private javax.swing.JTextField X7;
    private javax.swing.JTextField X8;
    private javax.swing.JTextField X9;
    // End of variables declaration//GEN-END:variables
}
